
window.onload=pantalla;/*lanzar varias funciones al cargar un html*/
var digitos="";

function pantalla(){
    var cero=document.getElementById("cero");
    cero.addEventListener("click",function(){
        digitar("0");
    });
    var uno=document.getElementById("uno");
    uno.addEventListener("click",function(){
        digitar("1");
    });
    var dos=document.getElementById("dos");
    dos.addEventListener("click", function(){
        digitar("2");
    });
    var tres=document.getElementById("tres");
    tres.addEventListener("click", function(){
        digitar("3");
    });
    var cuatro=document.getElementById("cuatro");
    cuatro.addEventListener("click", function(){
        digitar("4");
    });
    var cinco=document.getElementById("cinco");
    cinco.addEventListener("click", function(){
        digitar("5");
    });
    var seis=document.getElementById("seis");
    seis.addEventListener("click", function(){
        digitar("6");
    });
    var siete=document.getElementById("siete");
    siete.addEventListener("click", function(){
        digitar("7");
    });
    var ocho=document.getElementById("ocho");
    ocho.addEventListener("click", function(){
        digitar("8");
    });
    var nueve=document.getElementById("nueve");
    nueve.addEventListener("click", function(){
        digitar("9");
    });
    var divi=document.getElementById("division");
    divi.addEventListener("click", function(){
        digitar("/");
    });
    var multiplicacion=document.getElementById("multiplicacion");
    multiplicacion.addEventListener("click", function(){
        digitar("*");
    });
    var res=document.getElementById("resta");
    res.addEventListener("click", function(){
        digitar("-");
    });
    var sum=document.getElementById("suma");
    sum.addEventListener("click", function(){
        digitar("+");
    });
    var punto=document.getElementById("punto");
    punto.addEventListener("click", function(){
        digitar(".");
    });
    var raiz=document.getElementById("raiz");
    raiz.addEventListener("click", function(){
        raizc();
    });
    var ciento=document.getElementById("ciento");
    ciento.addEventListener("click", function(){
        digitar("%");
    });
    var fraccion=document.getElementById("fraccion");
    fraccion.addEventListener("click", function(){
        inversa();
    });
    var masmenos=document.getElementById("masmenos");
    masmenos.addEventListener("click", function(){
        digitar("-");
    });
    var back=document.getElementById("back");
    back.addEventListener("click", backspace);
    var digitos=document.getElementById("digitos");
    digitos.addEventListener("click", borrar);
    var igual=document.getElementById("igual");
    igual.addEventListener("click", calcul);
}

function digitar(num){
    digitos+=num;
    var resultado=document.getElementById("pantalla");
	resultado.textContent=digitos;
    if(digitos.length>12){
        alert("Pasó el límite de caracteres");
        borrar();
  }
}

function calcul(){
    var respuesta=eval(digitos);
    var resultado=document.getElementById("pantalla");
    resultado.textContent=respuesta;
}

function backspace(){
    var resl=document.getElementById("pantalla");
	var ter=digitos.slice(0,-1);
	digitos=ter;
	resl.textContent=ter;
}

function borrar(){
    digitos="";
  	var backspace=("");
	var resl=document.getElementById("pantalla");
	resl.textContent=backspace;
}

function raizc(){
    var numeros = document.getElementById("pantalla");
    var resultado = Math.sqrt(digitos);
    numeros.textContent=resultado;
}

function inversa(){
    var numeros = document.getElementById("pantalla");
    var resultado = (1 / digitos);
    if((digitos != "0") && (digitos != "")){
        numeros.textContent=resultado;
    }else if((digitos = "0") || (digitos = "")){
        alert("No se puede dividir entre cero");
        borrar();
    }
    
}